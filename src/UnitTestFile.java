/**
 * File: UnitTestFIle.java
 * Author: Kolger Hajati
 * Company: GalaxE.Solutions
 * Date: October 11, 2020
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UnitTestFile {
    public static void main(String[] args) {

    // Instantiate ArrayList
    List<Integer> numList = new ArrayList<>();

        int value = 0;
        int finalValue = 0;
        //Add to numList
        numList.add(4);
        numList.add(50);
        numList.add(23);
        numList.add(11);
        numList.add(22);

        // Iterator - traversing list and set
        Iterator<Integer> itr = numList.iterator();

        // Prints out the entire list
        while (itr.hasNext()) {
            value = itr.next();
            System.out.println("Value: " + value);
            if (value >= 4 && value <= 49) {
                itr.remove();
            }
        }
        finalValue = numList.get(0);
        System.out.println(finalValue);
    }
}
